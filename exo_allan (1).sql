-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le : ven. 11 fév. 2022 à 11:04
-- Version du serveur : 5.7.36
-- Version de PHP : 8.0.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `exo_allan`
--
CREATE DATABASE IF NOT EXISTS `exo_allan` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `exo_allan`;

-- --------------------------------------------------------

--
-- Structure de la table `comment`
--

DROP TABLE IF EXISTS `comment`;
CREATE TABLE IF NOT EXISTS `comment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `author_id` int(11) DEFAULT NULL,
  `game_id` int(11) DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `up_votes` int(11) DEFAULT NULL,
  `down_votes` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_9474526CF675F31B` (`author_id`),
  KEY `IDX_9474526CE48FD905` (`game_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `comment`
--

INSERT INTO `comment` (`id`, `author_id`, `game_id`, `slug`, `content`, `up_votes`, `down_votes`, `created_at`) VALUES
(1, 1, 1, '', 'ce jeu est un gros banger ce jeu est un gros banger ce jeu est un gros banger ce jeu est un gros banger ce jeu est un gros banger ce jeu est un gros banger ce jeu est un gros banger ce jeu est un gros banger ce jeu est un gros banger ce jeu est un gros banger ', NULL, NULL, '2022-02-08 12:38:53'),
(2, 2, 2, '', 'jeu de mort on fait que rager jpp', NULL, NULL, '2022-02-08 12:38:53'),
(3, 3, 3, '', 'tout le monde cheat + ya que des russes abyssal', NULL, NULL, '2022-02-08 12:38:53'),
(4, 4, 4, '', 'on dirait cs go en moins bien', NULL, NULL, '2022-02-08 12:38:53'),
(5, 5, 5, '', 'dommage qu\'il soit devenu nul par le free to play', NULL, NULL, '2022-02-08 12:38:53'),
(6, 6, 6, '', 'le jeu est trop simple imo', NULL, NULL, '2022-02-08 12:38:53'),
(7, 7, 7, '', 'C\'est LoL mais en version merde', NULL, NULL, '2022-02-08 12:38:53'),
(8, 4, 1, '', 'wallah jeoffrey il cheat il a déjà 1400 ilvl et lvl max alors que moi je viens juste de commencer le jeu', NULL, NULL, '2022-02-08 12:51:16');

-- --------------------------------------------------------

--
-- Structure de la table `doctrine_migration_versions`
--

DROP TABLE IF EXISTS `doctrine_migration_versions`;
CREATE TABLE IF NOT EXISTS `doctrine_migration_versions` (
  `version` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `executed_at` datetime DEFAULT NULL,
  `execution_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `doctrine_migration_versions`
--

INSERT INTO `doctrine_migration_versions` (`version`, `executed_at`, `execution_time`) VALUES
('DoctrineMigrations\\Version20220207102308', '2022-02-07 10:23:17', 1604),
('DoctrineMigrations\\Version20220208122735', '2022-02-08 12:27:51', 335);

-- --------------------------------------------------------

--
-- Structure de la table `game`
--

DROP TABLE IF EXISTS `game`;
CREATE TABLE IF NOT EXISTS `game` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `publisher_id` int(11) DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `coverimage_url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `logo_url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` double NOT NULL,
  `published_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_232B318C40C86FCE` (`publisher_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `game`
--

INSERT INTO `game` (`id`, `publisher_id`, `slug`, `name`, `description`, `coverimage_url`, `logo_url`, `price`, `published_at`) VALUES
(1, 1, 'lost-ark', 'Lost Ark', 'Lost Ark est un jeu vidéo de type MMORPG développé par Tripod Studio et édité par Smilegate RPG, sorti le 7 novembre 2018 sur Windows en Corée après trois bêta fermée. Le jeu est actuellement en bêta ouverte en Russie, la licence ayant été accordée à l\'éditeur russe Mail.ru.\r\n\r\nPublié par Amazon Games Studios, le jeu est également prévu pour 2022 en Europe, en Amérique du Nord, en Estonie et en Océanie, la première bêta fermée de ces régions ayant eu lieu du 4 au 11 novembre 2021.\r\n\r\nLe jeu a remporté six prix dans diverses catégories lors des Korea Game Awards 2019.', 'https://cdn.akamai.steamstatic.com/steam/apps/1599340/capsule_616x353.jpg?t=1642093292', '', 14.99, '2022-02-07 10:49:42'),
(2, 2, 'league-of(legends', 'League Of Legends', 'League of Legends (abrégé LoL) est un jeu vidéo sorti en 2009 de type arène de bataille en ligne, free-to-play, développé et édité par Riot Games sur Windows et Mac OS.\r\n\r\nLe mode principal du jeu voit s\'affronter deux équipes de 5 joueurs en temps réel dans des parties d\'une durée d\'environ une demi-heure, chaque équipe occupant et défendant sa propre base sur la carte. Chacun des dix joueurs contrôle un personnage à part entière parmi les plus de 150 qui sont proposés. Ces personnages, connus sous le nom de « champions » dans le jeu, disposent de compétences uniques et d\'un style de jeu qui leur est propre. Ils gagnent en puissance au fil de la partie en amassant des points d\'expérience ainsi qu\'en achetant des objets, dans le but de battre l\'équipe adverse. L\'objectif d\'une partie est de détruire le « Nexus » ennemi, une large structure située au centre de chaque base. D\'autres modes de jeu, généralement moins compétitifs et se basant quasiment toujours sur le mode principal, sont également présents — à l\'exception de Teamfight Tactics, un auto battler sorti en 2019 sans grand rapport avec le mode principal et qui dispose de sa propre communauté.\r\n\r\nInitialement inspiré de Defense of the Ancients, un ancien mod de Warcraft III, le jeu est publié le 27 octobre 2009 et adopte dès sa sortie un modèle économique « freemium ». Il est souvent considéré comme le jeu vidéo ayant la plus large scène compétitive au monde, ses compétitions étant internationales et réunissant d\'importantes audiences. Le Championnat du monde de League of Legends 2019 (en), par exemple, réunissait plus de 44 millions de spectateurs simultanés lors de ses pics de popularité, en novembre 2019.\r\n\r\nLeague of Legends a reçu des critiques généralement positives de la critique, gagnant des prix pour son accessibilité, le design de ses personnages et sa compétitivité. En juillet 2012, il était le premier jeu vidéo sur ordinateur en nombre d\'heures jouées en Europe et aux États-Unis. Son importante popularité a conduit à la création de produits dérivés tels que des clips musicaux, des bandes dessinées, des nouvelles, des figurines, et d\'une série d\'animation nommée Arcane.\r\n\r\nLe succès du jeu a également donné naissance à plusieurs autres jeux vidéo situés dans le même univers, tels que Legends of Runeterra, un jeu de cartes à collectionner, Ruined King: A League of Legends Story, un jeu de rôle au tour par tour, et League of Legends: Wild Rift, une adaptation sur mobile et console de LoL.', 'https://img.redbull.com/images/c_limit,w_1500,h_1000,f_auto,q_auto/redbullcom/2019/11/08/fc0f91de-3998-4be5-b50e-96d8d9e0cd45/lol-10-ans-league-of-legends', '', 0, '2022-02-07 10:49:42'),
(3, 1, 'counter-strike-global-offensive', 'Counter Strike : Global Offensive', 'Counter-Strike: Global Offensive (abrégé CS:GO) est un jeu de tir à la première personne multijoueur en ligne basé sur le jeu d\'équipe développé par Valve Corporation. Il est sorti le 21 août 2012 sur PC et consoles (Xbox 360, PlayStation 3)1. En 2017, Microsoft annonce que le jeu sur Xbox 360 sera compatible avec la Xbox One. Depuis le 6 décembre 2018, le jeu est disponible partiellement gratuitement en free-to-play2.', 'http://media.steampowered.com/apps/csgo/blog/images/fb_image.png?v=6', '', 20, '2022-02-07 12:48:23'),
(4, 2, 'valorant', 'Valorant', 'Valorant est un jeu vidéo free-to-play de tir à la première personne en multijoueur développé et édité par Riot Games et sorti le 2 juin 2020', 'https://cdn.pocket-lint.com/r/s/1200x630/assets/images/152432-games-feature-what-is-valorant-a-guide-to-the-free-to-play-fps-with-tips-on-how-to-win-image3-muha6tfgev.jpg', '', 0, '2022-02-07 12:48:23'),
(5, 1, 'aion', 'Aion', 'Aion: The Tower of Eternity souvent abrégé en Aion est un jeu vidéo sud-coréen de type jeu de rôle en ligne massivement multijoueur, édité et développé par NCsoft. Il est sorti en novembre 2008 en Corée, en avril 2009 en Chine, puis en septembre 2009 en Amérique du Nord et en Europe', 'https://secure-asset-delivery.gameforge.com/partnersite_live_product/0d0dff6e-2b2c-445f-9b37-7975ec9f1e9b/psKJ9RFeI_Q_big.jpg', '', 30, '2022-02-07 12:56:47'),
(6, 2, 'dark-souls', 'Dark souls', 'Dark Souls III est un jeu vidéo d\'Action-RPG de dark fantasy développé par FromSoftware. Ce troisième opus de la série Dark Souls est sorti sur Microsoft Windows, PlayStation 4 et Xbox One en mars 2016 au Japon et en avril en Amérique du Nord et en Europe', 'https://h3v2h5f6.rocketcdn.me/wp-content/uploads/2022/01/exploit-rce-dark-souls-iii.jpg', '', 40.99, '2022-02-07 12:56:47'),
(7, 2, 'dota', 'dota', 'Dota 2 est un jeu vidéo de type arène de bataille en ligne multijoueur développé et édité par Valve Corporation avec l\'aide de certains des créateurs du jeu d\'origine : Defense of the Ancients, un mod de carte personnalisée pour le jeu de stratégie en temps réel Warcraft III: Reign of Chaos et son extension Warcraft III: The Frozen Throne. Le jeu est sorti en juillet 2013 sur Microsoft Windows, OS X et Linux mettant fin à une phase bêta commencée en 2011. Il est disponible exclusivement sur la plateforme de jeu en ligne Steam.\r\n\r\nDota 2 se joue en matches indépendants opposant deux équipes de cinq joueurs, chacune possédant une base située en coin de carte contenant un bâtiment appelé l\'« Ancient », dont la destruction mène à la victoire de l\'équipe ennemie. Chaque joueur contrôle un « Héros » et est amené à accumuler de l’expérience, gagner de l\'or, s\'équiper d\'objets et combattre l\'équipe ennemie pour parvenir à la victoire.\r\n\r\nLe développement de Dota 2 commence en 2009, lorsque Valve embauche IceFrog, programmeur principal de Defense of the Ancients. Le jeu reçoit de très bonnes critiques à sa sortie dues à son système de jeu gratifiant, sa production de qualité et sa fidélité au mod originel. Il fut néanmoins critiqué pour sa courbe d\'apprentissage raide et sa communauté peu accueillante. Dota 2 a longtemps été le jeu le plus joué sur Steam, avec des pics quotidiens de plus de 900 000 joueurs connectés en même temps1, ainsi que plus de 13 000 000 de joueurs mensuels2.', 'https://images.ladepeche.fr/api/v1/images/view/61683f2b8fe56f1e277d5027/large/image.jpg?v=1', '', 0, '2022-02-07 12:59:23');

-- --------------------------------------------------------

--
-- Structure de la table `game_genre`
--

DROP TABLE IF EXISTS `game_genre`;
CREATE TABLE IF NOT EXISTS `game_genre` (
  `game_id` int(11) NOT NULL,
  `genre_id` int(11) NOT NULL,
  PRIMARY KEY (`game_id`,`genre_id`),
  KEY `IDX_B1634A77E48FD905` (`game_id`),
  KEY `IDX_B1634A774296D31F` (`genre_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `game_genre`
--

INSERT INTO `game_genre` (`game_id`, `genre_id`) VALUES
(1, 2),
(2, 3),
(3, 1),
(4, 1),
(5, 2),
(6, 2),
(7, 3);

-- --------------------------------------------------------

--
-- Structure de la table `game_lib_data`
--

DROP TABLE IF EXISTS `game_lib_data`;
CREATE TABLE IF NOT EXISTS `game_lib_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `is_installed` tinyint(1) NOT NULL,
  `played_time` time NOT NULL,
  `game_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_24C446B9E48FD905` (`game_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `game_lib_data`
--

INSERT INTO `game_lib_data` (`id`, `is_installed`, `played_time`, `game_id`) VALUES
(1, 1, '00:00:00', 1),
(2, 1, '00:00:00', 2),
(3, 1, '00:00:00', 3),
(4, 1, '00:00:00', 4),
(5, 1, '00:00:00', 5),
(6, 1, '00:00:00', 6),
(7, 1, '00:00:00', 7);

-- --------------------------------------------------------

--
-- Structure de la table `game_lib_data_library`
--

DROP TABLE IF EXISTS `game_lib_data_library`;
CREATE TABLE IF NOT EXISTS `game_lib_data_library` (
  `game_lib_data_id` int(11) NOT NULL,
  `library_id` int(11) NOT NULL,
  PRIMARY KEY (`game_lib_data_id`,`library_id`),
  KEY `IDX_8592216896E4EEA1` (`game_lib_data_id`),
  KEY `IDX_85922168FE2541D7` (`library_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `game_lib_data_library`
--

INSERT INTO `game_lib_data_library` (`game_lib_data_id`, `library_id`) VALUES
(1, 1),
(1, 2),
(1, 3),
(1, 4),
(1, 7),
(1, 9),
(2, 1),
(2, 2),
(2, 4),
(2, 6),
(2, 9),
(3, 1),
(3, 3),
(3, 7),
(3, 9),
(4, 1),
(4, 6),
(4, 9),
(5, 2),
(5, 5),
(5, 9),
(6, 3),
(6, 5),
(6, 6),
(6, 9),
(7, 3),
(7, 7),
(7, 8),
(7, 9);

-- --------------------------------------------------------

--
-- Structure de la table `genre`
--

DROP TABLE IF EXISTS `genre`;
CREATE TABLE IF NOT EXISTS `genre` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `genre`
--

INSERT INTO `genre` (`id`, `slug`, `name`) VALUES
(1, 'fps', 'Fps'),
(2, 'mmorpg', 'Mmorpg'),
(3, 'moba', 'Moba'),
(4, 'simulation', 'Simulation'),
(5, 'horror', 'horror');

-- --------------------------------------------------------

--
-- Structure de la table `library`
--

DROP TABLE IF EXISTS `library`;
CREATE TABLE IF NOT EXISTS `library` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `player_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_A18098BC99E6F5DF` (`player_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `library`
--

INSERT INTO `library` (`id`, `player_id`) VALUES
(1, 1),
(2, 2),
(3, 3),
(4, 4),
(5, 5),
(6, 6),
(7, 7),
(8, 8),
(9, 9),
(10, 11);

-- --------------------------------------------------------

--
-- Structure de la table `library_game_lib_data`
--

DROP TABLE IF EXISTS `library_game_lib_data`;
CREATE TABLE IF NOT EXISTS `library_game_lib_data` (
  `library_id` int(11) NOT NULL,
  `game_lib_data_id` int(11) NOT NULL,
  PRIMARY KEY (`library_id`,`game_lib_data_id`),
  KEY `IDX_979E6624FE2541D7` (`library_id`),
  KEY `IDX_979E662496E4EEA1` (`game_lib_data_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `library_game_lib_data`
--

INSERT INTO `library_game_lib_data` (`library_id`, `game_lib_data_id`) VALUES
(1, 3),
(1, 5);

-- --------------------------------------------------------

--
-- Structure de la table `publisher`
--

DROP TABLE IF EXISTS `publisher`;
CREATE TABLE IF NOT EXISTS `publisher` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `publisher`
--

INSERT INTO `publisher` (`id`, `slug`, `name`, `created_at`) VALUES
(1, 'amazon-games', 'AmazonGames', '2022-02-07 10:48:30'),
(2, 'riot', 'Riot', '2022-02-07 10:48:30'),
(3, 'gameforge', 'GameForge', '2017-01-01 00:00:00'),
(4, 'editeur-a-la-con', 'Editeur à la con', '2022-01-01 00:00:00'),
(5, 'editeur-a-la-con', 'Editeur à la con', '2022-01-01 00:00:00');

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `user`
--

INSERT INTO `user` (`id`, `username`, `email`, `slug`) VALUES
(1, 'Jeoffrey', 'jeoffreydugay@hotmail.fr', 'jeoffrey'),
(2, 'Allan', 'Allanjarry@hotmail.fr', 'allan'),
(3, 'Joao', 'joao@drosalys.fr', 'joao'),
(4, 'Ayoub', 'ayoub@drosalys.fr', 'ayoub'),
(5, 'Sophana', 'sophana@drosalys.fr', 'sophana'),
(6, 'Walid', 'walid@drosalys.fr', 'walid'),
(7, 'Mickaël', 'mickael@drosalys.fr', 'mickael'),
(8, 'Joëlle', 'joelle@drosalys.fr', 'joelle'),
(9, 'Kevin', 'kevin@drosalys.fr', 'kevin'),
(11, 'Corentin', 'Corentinjousson@hotmail.com', 'corentin');

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `comment`
--
ALTER TABLE `comment`
  ADD CONSTRAINT `FK_9474526CE48FD905` FOREIGN KEY (`game_id`) REFERENCES `game` (`id`),
  ADD CONSTRAINT `FK_9474526CF675F31B` FOREIGN KEY (`author_id`) REFERENCES `user` (`id`);

--
-- Contraintes pour la table `game`
--
ALTER TABLE `game`
  ADD CONSTRAINT `FK_232B318C40C86FCE` FOREIGN KEY (`publisher_id`) REFERENCES `publisher` (`id`);

--
-- Contraintes pour la table `game_genre`
--
ALTER TABLE `game_genre`
  ADD CONSTRAINT `FK_B1634A774296D31F` FOREIGN KEY (`genre_id`) REFERENCES `genre` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_B1634A77E48FD905` FOREIGN KEY (`game_id`) REFERENCES `game` (`id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `game_lib_data`
--
ALTER TABLE `game_lib_data`
  ADD CONSTRAINT `FK_24C446B9E48FD905` FOREIGN KEY (`game_id`) REFERENCES `game` (`id`);

--
-- Contraintes pour la table `game_lib_data_library`
--
ALTER TABLE `game_lib_data_library`
  ADD CONSTRAINT `FK_8592216896E4EEA1` FOREIGN KEY (`game_lib_data_id`) REFERENCES `game_lib_data` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_85922168FE2541D7` FOREIGN KEY (`library_id`) REFERENCES `library` (`id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `library`
--
ALTER TABLE `library`
  ADD CONSTRAINT `FK_A18098BC99E6F5DF` FOREIGN KEY (`player_id`) REFERENCES `user` (`id`);

--
-- Contraintes pour la table `library_game_lib_data`
--
ALTER TABLE `library_game_lib_data`
  ADD CONSTRAINT `FK_979E662496E4EEA1` FOREIGN KEY (`game_lib_data_id`) REFERENCES `game_lib_data` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_979E6624FE2541D7` FOREIGN KEY (`library_id`) REFERENCES `library` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
