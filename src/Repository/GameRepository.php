<?php

namespace App\Repository;

use App\Entity\Game;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Game|null find($id, $lockMode = null, $lockVersion = null)
 * @method Game|null findOneBy(array $criteria, array $orderBy = null)
 * @method Game[]    findAll()
 * @method Game[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class GameRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Game::class);
    }

    /**
     * @return Game[] Returns an array of Game objects
     */
    public function findByAlphaName()
    {
        return $this->createQueryBuilder('g')
            ->orderBy('g.name', 'ASC')
            ->getQuery()
            ->getResult();
    }

    /**
     * @return Game[] Returns an array of Game objects
     */
    public function findByCommented()
    {
        return $this->createQueryBuilder('g')
            ->addSelect('count(c) as hidden nbcomments')
            ->join('g.comments', 'c')
            ->orderBy('nbcomments', 'desc')
            ->groupBy('c.id')
            ->getQuery()
            ->getResult();
    }

    /**
     * @return Game[] Returns an array of Game objects
     */
    public function findByPriceASC()
    {
        return $this->createQueryBuilder('g')
            ->orderBy('g.price', 'ASC')
            ->getQuery()
            ->getResult();
    }

    /**
     * @return Game[] Returns an array of Game objects
     */
    public function findByPriceDESC()
    {
        return $this->createQueryBuilder('g')
            ->orderBy('g.price', 'DESC')
            ->getQuery()
            ->getResult();
    }

    /**
     * @return Game[] Returns an array of Game objects
     */
    public function findByLatest()
    {
        return $this->createQueryBuilder('g')
            ->orderBy('g.publishedAt', 'DESC')
            ->getQuery()
            ->getResult();
    }


    /*
    public function findOneBySomeField($value): ?Game
    {
        return $this->createQueryBuilder('g')
            ->andWhere('g.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
