<?php

namespace App\Repository;

use App\Entity\GameLibData;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method GameLibData|null find($id, $lockMode = null, $lockVersion = null)
 * @method GameLibData|null findOneBy(array $criteria, array $orderBy = null)
 * @method GameLibData[]    findAll()
 * @method GameLibData[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class GameLibDataRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, GameLibData::class);
    }

    // /**
    //  * @return GameLibData[] Returns an array of GameLibData objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('g')
            ->andWhere('g.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('g.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?GameLibData
    {
        return $this->createQueryBuilder('g')
            ->andWhere('g.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
