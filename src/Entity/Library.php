<?php

namespace App\Entity;

use App\Repository\LibraryRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: LibraryRepository::class)]
class Library
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\OneToOne(inversedBy: 'library', targetEntity: User::class, cascade: ['persist', 'remove'])]
    private $player;

    #[ORM\ManyToMany(targetEntity: GameLibData::class, mappedBy: 'library')]
    private $gameLibData;

    #[ORM\ManyToMany(targetEntity: GameLibData::class)]
    private $lastPlayedGame;

    public function __construct()
    {
        $this->gameLibData = new ArrayCollection();
        $this->lastPlayedGame = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPlayer(): ?User
    {
        return $this->player;
    }

    public function setPlayer(?User $player): self
    {
        $this->player = $player;

        return $this;
    }

    /**
     * @return Collection|GameLibData[]
     */
    public function getGameLibData(): Collection
    {
        return $this->gameLibData;
    }

    public function addGameLibData(GameLibData $gameLibData): self
    {
        if (!$this->gameLibData->contains($gameLibData)) {
            $this->gameLibData[] = $gameLibData;
            $gameLibData->addLibrary($this);
        }

        return $this;
    }

    public function removeGameLibData(GameLibData $gameLibData): self
    {
        if ($this->gameLibData->removeElement($gameLibData)) {
            $gameLibData->removeLibrary($this);
        }

        return $this;
    }

    /**
     * @return Collection|GameLibData[]
     */
    public function getLastPlayedGame(): Collection
    {
        return $this->lastPlayedGame;
    }

    public function addLastPlayedGame(GameLibData $lastPlayedGame): self
    {
        if (!$this->lastPlayedGame->contains($lastPlayedGame)) {
            $this->lastPlayedGame[] = $lastPlayedGame;
        }

        return $this;
    }

    public function removeLastPlayedGame(GameLibData $lastPlayedGame): self
    {
        $this->lastPlayedGame->removeElement($lastPlayedGame);

        return $this;
    }
}
