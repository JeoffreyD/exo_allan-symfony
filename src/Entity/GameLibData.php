<?php

namespace App\Entity;

use App\Repository\GameLibDataRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: GameLibDataRepository::class)]
class GameLibData
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'boolean')]
    private $isInstalled;

    #[ORM\Column(type: 'time')]
    private $playedTime;

    #[ORM\ManyToMany(targetEntity: Library::class, inversedBy: 'gameLibData')]
    private $library;

    #[ORM\ManyToOne(targetEntity: Game::class, inversedBy: 'gameLibData')]
    private $game;

    public function __construct()
    {
        $this->library = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIsInstalled(): ?bool
    {
        return $this->isInstalled;
    }

    public function setIsInstalled(bool $isInstalled): self
    {
        $this->isInstalled = $isInstalled;

        return $this;
    }

    public function getPlayedTime(): ?\DateTimeInterface
    {
        return $this->playedTime;
    }

    public function setPlayedTime(\DateTimeInterface $playedTime): self
    {
        $this->playedTime = $playedTime;

        return $this;
    }


    /**
     * @return Collection|Library[]
     */
    public function getLibrary(): Collection
    {
        return $this->library;
    }

    public function addLibrary(Library $library): self
    {
        if (!$this->library->contains($library)) {
            $this->library[] = $library;
        }

        return $this;
    }

    public function removeLibrary(Library $library): self
    {
        $this->library->removeElement($library);

        return $this;
    }

    public function getGame(): ?Game
    {
        return $this->game;
    }

    public function setGame(?Game $game): self
    {
        $this->game = $game;

        return $this;
    }
}
