<?php

namespace App\Entity;

use App\Repository\GameRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: GameRepository::class)]
class Game
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 255)]
    private $slug;

    #[ORM\Column(type: 'string', length: 255)]
    private $name;

    #[ORM\Column(type: 'text')]
    private $description;

    #[ORM\Column(type: 'string', length: 255)]
    private $coverimageUrl;

    #[ORM\Column(type: 'string', length: 255)]
    private $logoUrl;

    #[ORM\Column(type: 'float')]
    private $price;

    #[ORM\Column(type: 'datetime')]
    private $publishedAt;

    #[ORM\OneToMany(mappedBy: 'game', targetEntity: Comment::class)]
    private $comments;

    #[ORM\ManyToOne(targetEntity: Publisher::class, inversedBy: 'games')]
    private $publisher;

    #[ORM\ManyToMany(targetEntity: Genre::class, inversedBy: 'games')]
    private $genres;

    #[ORM\OneToMany(mappedBy: 'game', targetEntity: GameLibData::class)]
    private $gameLibData;


    public function __construct()
    {
        $this->comments = new ArrayCollection();
        $this->genres = new ArrayCollection();
        $this->gameLibData = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getCoverimageUrl(): ?string
    {
        return $this->coverimageUrl;
    }

    public function setCoverimageUrl(string $coverimageUrl): self
    {
        $this->coverimageUrl = $coverimageUrl;

        return $this;
    }

    public function getLogoUrl(): ?string
    {
        return $this->logoUrl;
    }

    public function setLogoUrl(string $logoUrl): self
    {
        $this->logoUrl = $logoUrl;

        return $this;
    }

    public function getPrice(): ?float
    {
        return $this->price;
    }

    public function setPrice(float $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getPublishedAt(): ?\DateTimeInterface
    {
        return $this->publishedAt;
    }

    public function setPublishedAt(\DateTimeInterface $publishedAt): self
    {
        $this->publishedAt = $publishedAt;

        return $this;
    }

    /**
     * @return Collection|Comment[]
     */
    public function getComments(): Collection
    {
        return $this->comments;
    }

    public function addComment(Comment $comment): self
    {
        if (!$this->comments->contains($comment)) {
            $this->comments[] = $comment;
            $comment->setGame($this);
        }

        return $this;
    }

    public function removeComment(Comment $comment): self
    {
        if ($this->comments->removeElement($comment)) {
            // set the owning side to null (unless already changed)
            if ($comment->getGame() === $this) {
                $comment->setGame(null);
            }
        }

        return $this;
    }

    public function getPublisher(): ?Publisher
    {
        return $this->publisher;
    }

    public function setPublisher(?Publisher $publisher): self
    {
        $this->publisher = $publisher;

        return $this;
    }

    /**
     * @return Collection|Genre[]
     */
    public function getGenres(): Collection
    {
        return $this->genres;
    }

    public function addGenre(Genre $genre): self
    {
        if (!$this->genres->contains($genre)) {
            $this->genres[] = $genre;
        }

        return $this;
    }

    public function removeGenre(Genre $genre): self
    {
        $this->genres->removeElement($genre);

        return $this;
    }

    /**
     * @return Collection|GameLibData[]
     */
    public function getGameLibData(): Collection
    {
        return $this->gameLibData;
    }

    public function addGameLibData(GameLibData $gameLibData): self
    {
        if (!$this->gameLibData->contains($gameLibData)) {
            $this->gameLibData[] = $gameLibData;
            $gameLibData->setGame($this);
        }

        return $this;
    }

    public function removeGameLibData(GameLibData $gameLibData): self
    {
        if ($this->gameLibData->removeElement($gameLibData)) {
            // set the owning side to null (unless already changed)
            if ($gameLibData->getGame() === $this) {
                $gameLibData->setGame(null);
            }
        }

        return $this;
    }
}
