<?php

namespace App\Form;

use App\Entity\Game;
use App\Repository\GameRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AddGameType extends AbstractType
{

    private GameRepository $gameRepository;

    /**
     * @param GameRepository $gameRepository
     */
    public function __construct(GameRepository $gameRepository)
    {
        $this->gameRepository = $gameRepository;
    }


    public function buildForm(FormBuilderInterface $builder, array $options): void
    {

        $gameEntity = $this->gameRepository->findAll();


        $builder
            ->add('game', EntityType::class, [
                'class' => Game::class,
                'label' => "Jeu à ajouter à cette utilisateur",
                'choice_label' => 'name',
                'multiple' => 'true',
                'expanded' => 'true'
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            // Configure your form options here
        ]);
    }
}
