<?php

namespace App\Form;

use App\Entity\Game;
use App\Entity\Genre;
use App\Entity\Publisher;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class GameType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name', TextType::class,[
                'label' => "Nom de l'utilisateur"
            ])
            ->add('description', TextareaType::class,[
                'label' => 'Description du Jeu'
            ])
            ->add('coverimageUrl', TextType::class,[
                'label' => "Url de l'image"
            ])
            ->add('logoUrl', TextType::class, [
                'label' => "Url du logo"
            ])
            ->add('price', MoneyType::class, [
                'label' => 'Prix du jeu'
            ])
            ->add('publishedAt', DateType::class, [
                'label' => 'Date de publication',
                'widget' => "choice"
            ])
            ->add('publisher', EntityType::class,[
                'class' => Publisher::class ,
                'choice_label' => 'name',
            ])
            ->add('genres', EntityType::class, [
                'class' => Genre::class,
                'choice_label' => 'name',
                'multiple' => 'true',
                'expanded' => 'true'
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Game::class,
        ]);
    }
}
