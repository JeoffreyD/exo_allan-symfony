<?php

namespace App\Controller;

use App\Repository\GenreRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class GenreController extends AbstractController
{

    private GenreRepository $genreRepository;

    /**
     * @param GenreRepository $genreRepository
     */
    public function __construct(GenreRepository $genreRepository)
    {
        $this->genreRepository = $genreRepository;
    }


    #[Route('/genres', name: 'genres')]
    public function genres(): Response
    {

        $genreEntities = $this->genreRepository->findAll();


        return $this->render('genre/index.html.twig', [
            'genres' => $genreEntities,
        ]);
    }

    #[Route('/genres/{id}', name: 'genre')]
    public function genre(int $id): Response
    {

        $genreEntity = $this->genreRepository->find($id);


        return $this->render('genre/games.html.twig', [
            'genreEntity' => $genreEntity,
        ]);
    }
}
