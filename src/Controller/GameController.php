<?php

namespace App\Controller;

use App\Repository\GameRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class GameController extends AbstractController
{
    private GameRepository $gameRepository;

    /**
     * @param GameRepository $gameRepository
     */
    public function __construct(GameRepository $gameRepository)
    {
        $this->gameRepository = $gameRepository;
    }


    #[Route('/', name: 'games')]
    public function games(): Response
    {
        $gameEntity = $this->gameRepository->findAll();

        return $this->render('game/index.html.twig', [
            'gameEntities' => $gameEntity,
        ]);
    }

    #[Route('/games/alpha-order', name: 'games-alpha-order')]
    public function gameAlphaName(): Response
    {
        $gameEntity = $this->gameRepository->findByAlphaName();

        return $this->render('game/index.html.twig', [
            'gameEntities' => $gameEntity,
        ]);
    }

    #[Route('/games/commented', name: 'games-comments')]
    public function gameComments(): Response
    {
        $gameEntity = $this->gameRepository->findByCommented();

        return $this->render('game/index.html.twig', [
            'gameEntities' => $gameEntity,
        ]);
    }

    #[Route('/games/latest', name: 'games-latest')]
    public function gameLast(): Response
    {
        $gameEntity = $this->gameRepository->findByLatest();

        return $this->render('game/index.html.twig', [
            'gameEntities' => $gameEntity,
        ]);
    }

    #[Route('/games/price-order-asc', name: 'games-price-order-asc')]
    public function gamePriceOrderAsc(): Response
    {
        $gameEntity = $this->gameRepository->findByPriceASC();

        return $this->render('game/index.html.twig', [
            'gameEntities' => $gameEntity,
        ]);
    }

    #[Route('/games/price-order-desc', name: 'games-price-order-desc')]
    public function gamePriceOrderDesc(): Response
    {
        $gameEntity = $this->gameRepository->findByPriceDESC();

        return $this->render('game/index.html.twig', [
            'gameEntities' => $gameEntity,
        ]);
    }

    #[Route('/games/{id}', name: 'game')]
    public function game(int $id): Response
    {
        $gameEntity = $this->gameRepository->find($id);

        return $this->render('game/game.html.twig', [
            'gameEntity' => $gameEntity,
        ]);
    }


}
