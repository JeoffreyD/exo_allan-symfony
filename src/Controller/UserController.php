<?php

namespace App\Controller;

use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class UserController extends AbstractController
{

    private UserRepository $userRepository;

    /**
     * @param UserRepository $userRepository
     */
    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }


    #[Route('/users', name: 'users')]
    public function index(): Response
    {

        $userEntity = $this->userRepository->findAll();

        return $this->render('user/index.html.twig', [
            'userEntities' => $userEntity,
        ]);
    }

    #[Route('/users/order-name', name: 'users-alpha')]
    public function index2(): Response
    {

        $userEntity = $this->userRepository->findByAlphaName();

        return $this->render('user/index.html.twig', [
            'userEntities' => $userEntity,
        ]);
    }

    #[Route('/users/order-games', name: 'users-games')]
    public function index3(): Response
    {

        $userEntity = $this->userRepository->findByNbGames();

        return $this->render('user/index.html.twig', [
            'userEntities' => $userEntity,
        ]);
    }

    #[Route('/users/{id}', name: 'user')]
    public function user(int $id): Response
    {
        $userEntity = $this->userRepository->find($id);

        return $this->render('user/user.html.twig', [
            'userEntity' => $userEntity,
        ]);
    }
}
