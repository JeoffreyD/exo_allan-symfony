<?php

namespace App\Controller\Admin;

use App\Entity\Game;
use App\Form\GameType;
use App\Repository\GameRepository;
use Cocur\Slugify\Slugify;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class GameController extends AbstractController
{

    private GameRepository $gameRepository;
    private EntityManagerInterface $entityManager;

    /**
     * @param GameRepository $gameRepository
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(GameRepository $gameRepository, EntityManagerInterface $entityManager)
    {
        $this->gameRepository = $gameRepository;
        $this->entityManager = $entityManager;
    }


    #[Route('/game/list', name: 'admin_game')]
    public function index(): Response
    {

        $gameEntities = $this->gameRepository->findAll();

        return $this->render('admin/game/list.html.twig', [
            'games' => $gameEntities,
        ]);
    }

    #[Route('/game/add', name: 'admin_add_game')]
    public function index2(Request $request): Response
    {

        $game = new Game();
        $form = $this->createForm(GameType::class, $game);

        $slugify = new Slugify();
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $game = $form->getData();
            $game->setSlug($slugify->slugify($game->getName()));

            $this->entityManager->persist($game);
            $this->entityManager->flush();

            $this->addFlash('success', 'Jeu crée');

        }

        return $this->render('admin/game/add.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    #[Route('/game/modify/{id}', name: 'admin_modify_game')]
    public function index3(Request $request, Game $game): Response
    {

        $form = $this->createForm(GameType::class, $game);

        $slugify = new Slugify();
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $game = $form->getData();
            $game->setSlug($slugify->slugify($game->getName()));

            $this->entityManager->persist($game);
            $this->entityManager->flush();

            $this->addFlash('success', 'Jeu modifié');

        }

        return $this->render('admin/game/add.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
