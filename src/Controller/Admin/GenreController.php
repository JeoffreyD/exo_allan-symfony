<?php

namespace App\Controller\Admin;

use App\Entity\Genre;
use App\Form\GenreType;
use App\Repository\GenreRepository;
use Cocur\Slugify\Slugify;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class GenreController extends AbstractController
{

    private GenreRepository $genreRepository;
    private EntityManagerInterface $entityManager;

    /**
     * @param GenreRepository $genreRepository
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(GenreRepository $genreRepository, EntityManagerInterface $entityManager)
    {
        $this->genreRepository = $genreRepository;
        $this->entityManager = $entityManager;
    }


    #[Route('/genre/list', name: 'admin_genre')]
    public function index(): Response
    {

        $genreEntities = $this->genreRepository->findAll();

        return $this->render('admin/genre/list.html.twig', [
            'genres' => $genreEntities,
        ]);
    }

    #[Route('/genre/add', name: 'admin_add_genre')]
    public function index2(Request $request): Response
    {

        $genre = new Genre();
        $form = $this->createForm(GenreType::class, $genre);

        $slugify = new Slugify();
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $genre = $form->getData();
            $genre->setSlug($slugify->slugify($genre->getName()));

            $this->entityManager->persist($genre);
            $this->entityManager->flush();

            $this->addFlash('success', 'Genre crée');

        }

        return $this->render('admin/genre/add.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
