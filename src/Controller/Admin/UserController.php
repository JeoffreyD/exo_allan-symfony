<?php

namespace App\Controller\Admin;

use App\Entity\GameLibData;
use App\Entity\Library;
use App\Entity\User;
use App\Form\AddGameType;
use App\Form\UserFormType;
use App\Repository\UserRepository;
use Cocur\Slugify\Slugify;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class UserController extends AbstractController
{
    private UserRepository $userRepository;
    private EntityManagerInterface $entityManager;

    /**
     * @param UserRepository $userRepository
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(UserRepository $userRepository, EntityManagerInterface $entityManager)
    {
        $this->userRepository = $userRepository;
        $this->entityManager = $entityManager;
    }


    #[Route('/user/list', name: 'admin_user')]
    public function index(): Response
    {
        $userEntities = $this->userRepository->findAll();

        return $this->render('admin/user/list.html.twig', [
            'users' => $userEntities,
        ]);
    }

    #[Route('/user/modify/{id}', name: 'admin_modify_user')]
    public function index3(Request $request, User $user): Response
    {

        $form = $this->createForm(UserFormType::class, $user);
        $formAddGame = $this->createForm(AddGameType::class);

        $slugify = new Slugify();
        $form->handleRequest($request);

        $formAddGame->handleRequest($request);

        if($formAddGame->isSubmitted() && $formAddGame->isValid()){
            $gamelist = $formAddGame->getData();
            foreach ($gamelist['game'] as $game ){
                $gameLib  = (new GameLibData())
                    ->setGame($game)
                    ->setIsInstalled(false)
                    ->setPlayedTime(new DateTime());
                $this->entityManager->persist($gameLib);

                $user->getLibrary()->addGameLibData($gameLib);
            }

            $this->entityManager->flush();

            $this->addFlash('successGame', "Les jeux de l'utilisateur on été modifiés");

        }

        if($form->isSubmitted() && $form->isValid()){
            $user = $form->getData();
            $user->setSlug($slugify->slugify($user->getUsername()));

            $this->entityManager->persist($user);
            $this->entityManager->flush();

            $this->addFlash('success', 'utilisateur modifié');

        }

        return $this->render('admin/user/add.html.twig', [
            'form' => $form->createView(),
            'formAddGame' => $formAddGame->createView()
        ]);
    }

    #[Route('/user/add', name: 'admin_add_user')]
    public function index2(Request $request): Response
    {

        $user = new User();
        $form = $this->createForm(UserFormType::class, $user);

        $library = new Library();
        $slugify = new Slugify();
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $user = $form->getData();
            $user->setSlug($slugify->slugify($user->getUsername()));

            $this->entityManager->persist($library);
            $user->setLibrary($library);
            $this->entityManager->persist($user);
            $this->entityManager->flush();

            $this->addFlash('success', 'Utilisateur crée');

        }

        return $this->render('admin/user/add.html.twig', [
            'form' => $form->createView(),
        ]);
    }


}
