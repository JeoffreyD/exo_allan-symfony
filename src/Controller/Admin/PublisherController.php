<?php

namespace App\Controller\Admin;

use App\Entity\Publisher;
use App\Form\PublisherType;
use App\Repository\PublisherRepository;
use Cocur\Slugify\Slugify;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class PublisherController extends AbstractController
{

    private EntityManagerInterface $entityManager;
    private PublisherRepository $publisherRepository;

    /**
     * @param EntityManagerInterface $entityManager
     * @param PublisherRepository $publisherRepository
     */
    public function __construct(EntityManagerInterface $entityManager, PublisherRepository $publisherRepository)
    {
        $this->entityManager = $entityManager;
        $this->publisherRepository = $publisherRepository;
    }


    #[Route('/publisher/add', name: 'admin_publisher')]
    public function index(Request $request): Response
    {

        $publisher = new Publisher();
        $form = $this->createForm(PublisherType::class, $publisher);

        $slugify = new Slugify();
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $publisher = $form->getData();
            $publisher->setSlug($slugify->slugify($publisher->getName()));

            $this->entityManager->persist($publisher);
            $this->entityManager->flush();

            $this->addFlash('success', 'Editeur crée');

        }

        return $this->render('admin/publisher/add.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    #[Route('/publisher/list', name: 'admin_publishers')]
    public function index2(): Response
    {

        $publisherEntities = $this->publisherRepository->findAll();

        return $this->render('admin/publisher/list.html.twig', [
            'publishers' => $publisherEntities,
        ]);
    }
}
