<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220208122735 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE game DROP FOREIGN KEY FK_232B318C96E4EEA1');
        $this->addSql('DROP INDEX IDX_232B318C96E4EEA1 ON game');
        $this->addSql('ALTER TABLE game DROP game_lib_data_id');
        $this->addSql('ALTER TABLE game_lib_data ADD game_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE game_lib_data ADD CONSTRAINT FK_24C446B9E48FD905 FOREIGN KEY (game_id) REFERENCES game (id)');
        $this->addSql('CREATE INDEX IDX_24C446B9E48FD905 ON game_lib_data (game_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE comment CHANGE slug slug VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE content content LONGTEXT NOT NULL COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE game ADD game_lib_data_id INT DEFAULT NULL, CHANGE slug slug VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE name name VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE description description LONGTEXT NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE coverimage_url coverimage_url VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE logo_url logo_url VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE game ADD CONSTRAINT FK_232B318C96E4EEA1 FOREIGN KEY (game_lib_data_id) REFERENCES game_lib_data (id)');
        $this->addSql('CREATE INDEX IDX_232B318C96E4EEA1 ON game (game_lib_data_id)');
        $this->addSql('ALTER TABLE game_lib_data DROP FOREIGN KEY FK_24C446B9E48FD905');
        $this->addSql('DROP INDEX IDX_24C446B9E48FD905 ON game_lib_data');
        $this->addSql('ALTER TABLE game_lib_data DROP game_id');
        $this->addSql('ALTER TABLE genre CHANGE slug slug VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE name name VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE publisher CHANGE slug slug VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE name name VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE `user` CHANGE username username VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE email email VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE slug slug VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`');
    }
}
